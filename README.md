```
npm i
npm start
```

Simple react starter for myself that uses an express server, webpack, and webpack-dashboard.

It should rebuild on any change, including a change to the webpack.config.js file while running.
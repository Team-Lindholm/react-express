const path = require('path');
const webpack = require('webpack');
const express = require('express');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const DashboardPlugin = require('webpack-dashboard/plugin');

const config = require('../webpack.config.js');

const isProduction = process.env.NODE_ENV === 'production';
const port = 3000 || process.env.PORT;

const app = express();

const index = path.join(__dirname, 'dist', 'index.html');
console.log(index);


if (!isProduction) {
    const compiler = webpack(config);
    compiler.apply(new DashboardPlugin());

    const devMiddleware = webpackDevMiddleware(compiler, {
        historyApiFallback: true,
        publicPath: config.output.publicPath,
        quiet: true,
        stats: {
            colors: true,
            hash: false,
            timings: false,
            chunks: false,
            chunkModules: false,
            modules: false,
        },
    });
    app.use(devMiddleware);
    app.use(webpackHotMiddleware(compiler));
    app.get('*', (req, res) => {
        res.write(compiler.outputFileSystem.readFileSync(index));
        res.end();
    });
} else {
    app.use(express.static(config.output.publicPath));
    app.get('*', (req, res) => {
        res.sendFile(index);
    });
}

app.listen(port, '0.0.0.0', (err) => {
    if (err) {
        console.log(err);
    }
    console.info(`==> Listening on port ${port}, open http:/0.0.0.0:${port} in your browser`);
});
